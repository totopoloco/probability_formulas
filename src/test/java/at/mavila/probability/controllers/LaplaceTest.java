package at.mavila.probability.controllers;

import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

class LaplaceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(LaplaceTest.class);

    @Test
    void diceSum() {
        Laplace laplace = new Laplace();
        final ResponseEntity<DecimalFractionResponse> stringResponseResponseEntity = laplace.diceProbability(2L, 3L);
        assertThat(Double.parseDouble(stringResponseResponseEntity.getBody().getDecimal())).isCloseTo(0.083333333333, Percentage.withPercentage(2D));
        LOGGER.info("{}", stringResponseResponseEntity.getBody());
    }
}