package at.mavila.probability;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class RationalTest {

    @Test
    public void testGetFractionHalfValue() {
        final String fraction = Rational.getFraction(new BigDecimal("0.5"));
        assertThat(fraction).isEqualTo("1/2");
    }

    @Test
    public void testGetFractionThreeQuartersValue() {
        final String fraction = Rational.getFraction(new BigDecimal("0.75"));
        assertThat(fraction).isEqualTo("3/4");
    }

    @Test
    public void testGetFractionEightyValue() {
        final String fraction = Rational.getFraction(new BigDecimal("0.80"));
        assertThat(fraction).isEqualTo("4/5");
    }

    @Test
    public void testGetFractionMoreDecimalsValue() {
        final String fraction = Rational.getFraction(new BigDecimal("0.73642"));
        assertThat(fraction).isEqualTo("366/497");
    }

    @Test
    public void testGetFractionMoreZerosValue() {
        final String fraction = Rational.getFraction(new BigDecimal("0.003844"));
        assertThat(fraction).isEqualTo("1/260");
    }

    @Test
    public void testGetFractionLongValue() {
        final String fraction = Rational.getFraction(new BigDecimal("0.847473784767434"));
        assertThat(fraction).isEqualTo("839/990");
    }
}
