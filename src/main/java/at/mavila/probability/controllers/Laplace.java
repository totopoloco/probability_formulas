package at.mavila.probability.controllers;

import at.mavila.probability.Constants;
import at.mavila.probability.Rational;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.BigDecimal.valueOf;
import static java.util.Objects.isNull;

@RestController
@Component
public class Laplace {

    private static final Logger LOGGER = LoggerFactory.getLogger(Laplace.class);

    @Value("${laplace.default.dices}")
    private long defaultDices;

    @Value("${laplace.default.positive_cases}")
    private long defaultPositiveCases;

    @GetMapping(value = "/hello")
    public String hello() {
        return "Hello World";
    }

    @GetMapping(value = "/diceProbability/{dices}/{positive_cases}", headers = "Accept=application/json")
    @ResponseBody
    public ResponseEntity<DecimalFractionResponse> diceProbability(
            @PathVariable(value = "dices", required = false)
                    Long dices,
            @PathVariable(value = "positive_cases", required = false)
                    Long positiveCases) {

        final long dicesLocal = isNull(dices) ? this.defaultDices : dices;
        final long positiveCasesLocal = isNull(positiveCases) ? this.defaultPositiveCases : positiveCases;

        LOGGER.info("Dices: <{}>", dicesLocal);
        LOGGER.info("Positive cases: <{}>", positiveCasesLocal);

        final long possibleCases = this.possibleCases(dicesLocal);

        if (positiveCases > possibleCases) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new DecimalFractionResponse(String.format(
                    "Positive cases cannot be bigger than possible cases. Possible cases: <%d> Positive cases: <%d>",
                    possibleCases, positiveCases), StringUtils.EMPTY));
        }

        BigDecimal bdPositiveCases = valueOf(positiveCasesLocal);
        BigDecimal bdPossibleCases = valueOf(possibleCases);

        BigDecimal divide = bdPositiveCases.divide(bdPossibleCases, 12, RoundingMode.HALF_UP);

        LOGGER.info("Returning: <{}>", divide);

        return ResponseEntity.ok(new DecimalFractionResponse(divide.toString(), Rational.getFraction(divide)));
    }

    private long possibleCases(long dices) {
        long result = 1;
        while (dices > 0) {

            result = Constants.DICE_FACES * result;

            --dices;
        }
        return result;
    }

}
