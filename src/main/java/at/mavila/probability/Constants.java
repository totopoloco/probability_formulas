package at.mavila.probability;

public final class Constants {

    private  Constants() {
    }

    public static final int DICE_FACES = 6;
}
