package at.mavila.probability.probability;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = {"at.mavila.probability.controllers"})
public class ProbabilityApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ProbabilityApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ProbabilityApplication.class);
	}
}
