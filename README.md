# probability_formulas


## Introduction
This is a Spring boot project, pre requisites

*  Java 11 or newer to run this application (OpenJDK. https://jdk.java.net/java-se-ri/11 or Oracle. https://www.oracle.com/java/technologies/javase-downloads.html#JDK11)
*  (Optional) Intellij is handy if you want to modify the code
*  (Optional) Gradle. Use the wrapper when Gradle is not already in the PATH

## Objective
Different implementation of algorithms as given in a course of probability and statistics

## Gradle wrapper
From the command line run the following command:

### In Windows
`gradlew bootRun`

### In Unix based systems
`./gradlew bootRun`

### Access the API
By default the application opens the port 8080 on localhost, in your browser (or curl command) type:

`http://localhost:8080/<api>/....`

(See below for a description of the algorithms implemented in this project)

## API
To be added more over time


| Name | Description | API | Example Usage |
| ------ | ------ | ------ | ------ |
| Laplace | Calculates the probability to throw dices having the positive cases known beforehand | Request: `/diceProbability/{dices}/{positive_cases}`<br /> Response: <br />`{"decimal":"[decimal]","fraction":"[fraction]"}`<br />(See the example) | What is the probability to throw a dice and get 1,2 or 3: <br />`curl -s -w '\n' -X GET 'http://localhost:8080/diceProbability/1/3'`<br />Response: `{"decimal":"0.500000000000","fraction":"1/2"}`<br /><hr />What is the probability to throw two dices and get the 1:1 combination: <br /> `curl -s -w '\n' -X GET 'http://localhost:8080/diceProbability/2/1'`<br />Response: `{"decimal":"0.027777777778","fraction":"1/36"}` |